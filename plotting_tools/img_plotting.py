#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 6 2018

@author: tsuchida
"""

import math
import os.path as op
import numpy as np
import nibabel as nib
from mriqc.viz.utils import _get_limits, plot_slice


def plot_sag_perframe(img, out_file=None, ncols=8, title=None, overlay_mask=None,
                bbox_mask_file=None, flabels=None,
                vmin=None, vmax=None, cmap='Greys_r', fig=None):


    if isinstance(img, (str, bytes)):
        nii = nib.as_closest_canonical(nib.load(img))
        img_data = np.asanyarray(nii.dataobj)
        zooms = nii.header.get_zooms()
    else:
        img_data = img
        zooms = [1.0, 1.0, 1.0]
        out_file = 'mosaic.svg'

    # Remove extra dimensions
    img_data = np.squeeze(img_data)

    # Reshape if only one frame
    if len(img_data.shape) == 3:
        img_data = img_data.reshape((img_data.shape)+(1,))

    # Get number of frames to plot
    nframes = img_data.shape[-1]

    if flabels is not None:
        assert len(flabels) == nframes, "Number of frame labels do not much the number of frames of the image"

    nrows = math.ceil(nframes / float(ncols))

    if overlay_mask:
        overlay_data = nib.as_closest_canonical(
            nib.load(overlay_mask)).get_data()

    # create figures
    if fig is None:
        fig = plt.figure(figsize=(22, nrows * 2.5))

    est_vmin, est_vmax = _get_limits(img_data,
                                     only_plot_noise=False)
    if not vmin:
        vmin = est_vmin
    if not vmax:
        vmax = est_vmax

    naxis = 1

    # Get mid-saggital x
    mid_x = int(img_data.shape[0] / 2.0)
    for frame in range(nframes):
        ax = fig.add_subplot(nrows, ncols, naxis)

        if overlay_mask:
            ax.set_rasterized(True)
        plot_slice(img_data[mid_x, :, :, frame], vmin=vmin, vmax=vmax,
                   cmap=cmap, ax=ax, spacing=[zooms[0], zooms[2]],
                   label='frame %d' % (frame if flabels is None else flabels[frame]))

        if overlay_mask:
            from matplotlib import cm
            msk_cmap = cm.Reds  # @UndefinedVariable
            msk_cmap._init()
            alphas = np.linspace(0, 0.75, msk_cmap.N + 3)
            msk_cmap._lut[:, -1] = alphas
            plot_slice(overlay_data[mid_x, :, :], vmin=0, vmax=1,
                       cmap=msk_cmap, ax=ax, spacing=[zooms[0], zooms[2]])
        naxis += 1

    fig.subplots_adjust(
        left=0.05, right=0.95, bottom=0.05, top=0.95, wspace=0.05,
        hspace=0.05)

    if title:
        fig.suptitle(title, fontsize='10')
    fig.subplots_adjust(wspace=0.002, hspace=0.002)

    if out_file is None:
        fname, ext = op.splitext(op.basename(img))
        if ext == ".gz":
            fname, _ = op.splitext(fname)
        out_file = op.abspath(fname + '_mosaic.svg')

    fig.savefig(out_file, format='svg', dpi=300, bbox_inches='tight')
    return out_file
