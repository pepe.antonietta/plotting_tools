#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 12:10:21 2018

@author: tsuchida
"""

from scipy.odr import Model, Data, ODR
import scipy.stats as stats


def orthoregress(x, y):
    """Perform an Orthogonal Distance Regression on the given data,
    using the same interface as the standard scipy.stats.linregress function.
    Adapted from https://gist.github.com/robintw/d94eb527c44966fbc8b9#file-orthoregress-py
    
    Arguments:
    x: x data
    y: y data

    Returns:
    [slope, intercept, residual]

    Uses standard ordinary least squares to estimate the starting parameters
    then uses the scipy.odr interface to the ODRPACK Fortran code to do the
    orthogonal distance calculations.
    """
      
    def f(p, x):
        """Basic linear regression 'model' for use with ODR"""
        return (p[0] * x) + p[1]
    
    linreg = stats.linregress(x, y)
    mod = Model(f)
    dat = Data(x, y)
    od = ODR(dat, mod, beta0=linreg[0:2])
    out = od.run()

    return list(out.beta) + [out.res_var]